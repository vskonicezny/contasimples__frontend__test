import React from "react";

import {
  Grid,
  Hidden,
  TextField,
  Button,
  InputAdornment,
} from "@material-ui/core";
import { useStyleLogin } from "../../styles/pages/pages.style";

import loginImg from "../../assets/images/backGround.jpg";
import logo from "../../assets/images/logo-complete.svg";
import { AccountCircleOutlined, LockOpenOutlined } from "@material-ui/icons";

function LoginPage(props) {
  const loginClasses = useStyleLogin();

  return (
    <div>
      <Grid container style={loginClasses.root}>
        <Hidden smDown>
          <Grid item sm={6}>
            <img
              src={loginImg}
              style={loginClasses.img}
              alt="trabalhando em um notebook aberto"
              srcset=""
            />
          </Grid>
        </Hidden>
        <Grid container item xs={12} sm={6} style={loginClasses.rside}>
          <div />
          <div style={loginClasses.form}>
            <Grid container justify="center">
              <img src={logo} alt="logo da marca" width={200} />
            </Grid>
            <TextField
              label="CNPJ"
              margin="normal"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <AccountCircleOutlined />
                  </InputAdornment>
                ),
              }}
              inputMode="numeric"
            />
            <TextField
              label="SENHA"
              type="password"
              margin="normal"
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start">
                    <LockOpenOutlined />
                  </InputAdornment>
                ),
              }}
            />
            <div style={{ height: 24 }} />
            <Button variant="text" color="primary" onClick={() => {}}>
              Entrar
            </Button>
            <div style={{ height: 16 }} />
            <Button variant="text" color="primary">
              Registrar
            </Button>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}

export default LoginPage;
