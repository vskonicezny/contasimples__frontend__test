// sobrepões configurações do json-server usado por cli

const jsonServer = require("json-server");
const apiServer = jsonServer.create();
const path = require("path");
const router = jsonServer.router(path.join(__dirname, "dataBase", "db.json"));

/**
 * Consfigurações utilizando os modelos apresentados na documentação
 * url:  https://github.com/typicode/json-server#api
 */

const middlewares = jsonServer.defaults({
  static: "node_modules/json-server/public",
});

apiServer.use(middlewares);
apiServer.use(jsonServer.bodyParser);
/**
 * Simular delay para ter um comportamento assíncrono
 * baseado no modelo: https://app.pluralsight.com/course-player?clipId=7234654d-c523-4a9c-9945-3291664063b6
 */
apiServer.use((req, res, next) => setTimeout(next, 0));

apiServer.post("/empresas/", function (req, res, next) {
  const error = validateEmpresa(req.body);
  if (error) {
    res.status(400).send(error);
  } else {
    req.body.slug = createSlug(req.body.nomeEmpresa); // Generate a slug for new courses.
    next();
  }
});

apiServer.post("/transacoes/", function (req, res, next) {
  const error = validateTransacoes(req.body);
  if (error) {
    res.status(400).send(error);
  } else {
    req.body.slug = createSlug(req.body.dataTransacao); // Generate a slug for new courses.
    next();
  }
});

apiServer.use(router);
const port = 3001;
apiServer.listen(port, () => {
  console.log(`Server rodando na porta ${port}`);
});

/**
 * Validando dados na inserção
 */

function createSlug(value) {
  return value
    .replace(/[^a-z0-9_]+/gi, "-")
    .replace(/^-|-$/g, "")
    .toLowerCase();
}

const validateEmpresa = (empresa) => {
  if (!empresa.nomeEmpresa) return "É necessário informar o nome da empresa.";
  if (!empresa.cnpj) return "É necessário informar o cnpj da empresa.";
  // object validator
  for (let dados in empresa.dadosBancario) {
    if (!empresa.dadosBancario[dados])
      return `É necessário informar o(a) ${dados}`;
  }
  return "Looks Good!";
};

const validateTransacoes = (transacao) => {
  if (!transacao.empresaId) return "É necessário informar o Id da empresa.";
  if (!transacao.dataTransacao)
    return "É necessário informar a data da transação.";
  if (!transacao.valor) return "É necessário informar o valor.";
  if (!transacao.finalCartao) return "É necessário informar o final do Cartao.";
  if (!transacao.tipoTransacao) return "É necessário informar o Id da empresa.";
  if (!transacao.estabelecimento)
    return "É necessário informar a estabelecimento da empresa.";
  if (transacao.credito === null || transacao.credito === undefined)
    return "É necessário informar o tipo de operação.";
};

/**
 *    "empresaId": 2,
      "dataTransacao": "2020-09-18T20:50:00",
      "valor": 154.85,
      "finalCartao": "0122",
      "tipoTransacao": "CARD",
      "descricaoTransacao": "Compra com cartão de crédito",
      "estabelecimento": "⁯INSTAGRAM",
      "credito": false
*/
/* "nomeEmpresa": "EMPRESA UM S/A",
"cnpj": "13182905000171",
"dadosBancario": {
  "banco": 999,
  "bancoNome": "CONTA SIMPLES",
  "agencia": 1,
  "conta": 123456,
  "digitoConta": "1"
},
"saldo": 10500.0
*/
