const fs = require("fs");
const path = require("path");
const mockData = require("../mocks/consollidateMockData");

const { empresas, transacoes } = mockData;
const stringfyedData = JSON.stringify({ empresas, transacoes });
const dataBasePath = path.join(__dirname, "dataBase", "db.json");

// creation feedBack and error handling

fs.writeFile(dataBasePath, stringfyedData, (error) => {
  if (error) {
    return console.log(error);
  }
  return console.log("Banco de dados criado com sucesso");
});
