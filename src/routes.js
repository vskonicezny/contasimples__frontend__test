import React from "react";
import { Switch, Route } from "react-router-dom";

import Landpage from "./pages/Landpage";
import LoginPage from "./pages/LoginPage";
import DashBoard from "./pages/DashBoard";
import AccountDetails from "./pages/AccountDetails";
import NotFound from "./pages/NotFound";
import Header from "./components/commoms/Header";

function Routes() {
  return (
    <>
      {!window.location.pathname === "/login" ? <Header /> : null}
      <Switch>
        <Route exact path="/" component={Landpage} />
        <Route path="/login" component={LoginPage} />
        <Route path="/dashboard" component={DashBoard} />
        <Route path="/details" component={AccountDetails} />
        <Route component={NotFound} />
      </Switch>
    </>
  );
}

export default Routes;
