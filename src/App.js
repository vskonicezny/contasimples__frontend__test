import React from "react";
import { BrowserRouter as Router } from "react-router-dom";

import Routes from "./routes";
import CssBaseline from "@material-ui/core/CssBaseline";

export default function App() {
  return (
    <Router>
      <CssBaseline />
      <Routes />
    </Router>
  );
}
