import { makeStyles } from "@material-ui/core/styles";

export const useStyleCard = makeStyles((theme) => ({
  root: {
    display: "flex",
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  layout: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  },
  title: {
    fontSize: 14,
  },
  avatar: {
    marginRight: theme.spacing(8),
    width: 50,
    height: 50,
  },
}));
