export const defaultLayoutstyles = () => ({
  root: {
    padding: "50px 100px",
    zIndex: 999,
  },
  rightBorder: {
    borderRight: "solid #d0D0D0 1px",
  },
  content: {
    marginTop: 0,
  },
  paper: {
    background: "#9de1fe",
    padding: 20,
  },
});
