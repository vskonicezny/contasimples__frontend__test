import { makeStyles } from "@material-ui/core/styles";

const drawerWidth = "20vw";

export const useStylesDrawer = makeStyles((theme) => ({
  root: {
    listStyle: "none",
    color: "#303030",
  },
  logo: {
    display: "flex",
    justifyContent: "spaceBetween",
  },
  smallText: {
    fontSize: theme.typography.caption,
  },
  drawer: {
    width: drawerWidth,
    height: "100%",
  },
  content: {
    padding: theme.spacing(2),
  },
  money: {
    margin: 0,
  },
}));
