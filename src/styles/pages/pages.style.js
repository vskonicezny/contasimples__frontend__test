export const useStyleLogin = () => ({
  root: {
    minHeight: "100vh",
  },
  img: {
    width: "100%",
    height: "100%",
    objectFit: "cover",
  },
  rside: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
  },
  form: {
    display: "flex",
    flexDirection: "column",
    minWidth: "300px",
  },
});
