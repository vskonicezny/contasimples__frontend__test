import { handleError, handleResponse } from "../apiHandlers";
const BASE_URL = `${process.env.REACT_APP_API_ENDPOINT}`;

export function createEmpresa(empresa) {
  return fetch(`${BASE_URL}/empresas/${empresa.empresaId}`, {
    method: empresa.empresaId ? "PUT" : "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(empresa),
  })
    .then(handleResponse)
    .catch(handleError);
}

export function authEmpresa({ cnpj }) {
  return fetch(`${BASE_URL}/empresas?cnpj`)
    .then(handleResponse)
    .catch(handleError);
}
