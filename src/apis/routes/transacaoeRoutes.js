import { handleError, handleResponse } from "../apiHandlers";
const BASE_URL = `${process.env.REACT_APP_API_ENDPOINT}`;

export function listTransacoes(empresa) {
  return fetch(`${BASE_URL}/transacoes/${empresa.empresaId}`)
    .then(handleResponse)
    .catch(handleError);
}

export function createTransaction(empresa) {
  return fetch(`${BASE_URL}/empresas/${empresa.empresaId}`, {
    method: empresa.empresaId ? "PUT" : "POST",
    headers: { "content-type": "application/json" },
    body: JSON.stringify(empresa),
  })
    .then(handleResponse)
    .catch(handleError);
}
