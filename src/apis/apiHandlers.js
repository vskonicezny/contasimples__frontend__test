export async function handleResponse(res) {
  try {
    if (res.ok) {
      return res.json();
    }
  } catch (error) {
    if (res.status === 400) {
      throw new Error(res.text);
    } else {
      throw new Error("Ocorreu um erro na resposta do server");
    }
  }
}

export function handleError(error) {
  console.log(`Ao chamar a API o seguinte erro ocorreu: ${error}`);
  throw error;
}
